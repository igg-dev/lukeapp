ShopifyApp.configure do |config|
  config.application_name = "OrderMetafields"
  config.api_key = "22c6475d57bbf43208d0419153538672"
  config.secret = "2a1162f86d3e3cfeec9650b84d06030a"
  config.scope = "read_orders, read_products, write_orders"
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
  config.webhooks = [
    { topic: 'orders/paid', address: 'https://ce3042e6.ngrok.io/webhooks/orders_paid', format: 'json'}]
end
