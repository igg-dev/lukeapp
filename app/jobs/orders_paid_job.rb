class OrdersPaidJob < ActiveJob::Base
  
  def perform(shop_domain:, webhook:)
    shop = Shop.find_by(shopify_domain: shop_domain)
    shop.with_shopify_session do
      # country_code = webhook[:customer][:default_address][:country_code]
      country_code = webhook[:shipping_address][:country_code]
      puts webhook 
      order_id = webhook[:id]
      order = ShopifyAPI::Order.find(order_id)

      if %w(AT BE BG ES HR CH CY CZ DE DK EE FI GB IE IT LT MT NL NO PL SK SI KG).include? country_code
        value = 'Online-EU'
      elsif  %w(MX US).include? country_code
        value = 'Online-USA'   
      elsif country_code=="CA"
        value = 'Online-CDN'      
      end
      
        order.add_metafield(ShopifyAPI::Metafield.new({
           :namespace => 'order-location',
           :key => 'megafield',
           :value => value,
           :value_type => 'string'
        }))
    end
  end
end